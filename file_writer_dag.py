from airflow import DAG
from datetime import datetime
from transaction_generator import *
from airflow.operators.python import PythonOperator


CSV_DIR = "/home/axelle/Documents/Transactions_bancaires/"

dag = DAG(dag_id="client_writter",
         start_date=datetime(23,1,1),
         schedule_interval='*/15 * * * *',
         catchup=False)

write_client_datas = PythonOperator(
    task_id='write_client_data',
    python_callable=generate_transaction,
    op_kwargs={
        'file_name': CSV_DIR + "new_transaction_" + datetime.now().strftime("%d-%m-%Y_%H:%M:%S") + ".csv",
        'nrows' : 1000
        },
    dag=dag,
)


write_client_datas