from airflow import DAG
from airflow.operators.python import PythonOperator
from airflow.contrib.sensors.file_sensor import FileSensor
from datetime import datetime
import pandas as pd
from pipeline import *
import clustering as clust
import os

CSV_DIR = '/home/axelle/Documents/Transactions_bancaires/'
PIPELINE = Pipeline()

def get_csv_files():
    csv_files = [os.path.join(CSV_DIR, f) for f in os.listdir(CSV_DIR) if f.endswith('.csv')]
    return csv_files


def data_process(json):
    newJson = PIPELINE.handle_missing_values(json)
    return PIPELINE.handle_outliers(newJson)


def save_to_db():
    PIPELINE.write_to_db()


dag = DAG(dag_id="data_pipeline",
         start_date=datetime(23,1,1),
         schedule_interval='*/15 * * * *',
         #timedelta(minutes=15),
         catchup=False)

# Sensor to check for new CSV files
csv_getter = PythonOperator(
    task_id='get_csv_files',
    python_callable=get_csv_files,
    dag=dag,
)
         
check_file_integrety = PythonOperator(
    task_id='check_file_integrety',
    python_callable=PIPELINE.file_integrity,
    op_kwargs={'file': '{{ ti.xcom_pull(task_ids="get_csv_files")[0] }}'},
    dag=dag,
)

data_process = PythonOperator(
            task_id="data_process",
            python_callable=data_process,
            op_kwargs={'json': '{{ ti.xcom_pull(task_ids="check_file_integrety") }}'},
            dag=dag
        )

save_to_db = PythonOperator(
            task_id="save_to_db",
            python_callable=PIPELINE.write_to_db,
            op_kwargs={'datas': '{{ ti.xcom_pull(task_ids="data_process") }}'},
            dag=dag
        )

clustering_task = PythonOperator(
            task_id="clustering",
            python_callable=clust.main,
            dag=dag
        )

csv_getter >> check_file_integrety >> data_process >> save_to_db >> clustering_task