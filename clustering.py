from sklearn.preprocessing import OneHotEncoder
from sklearn.cluster import KMeans
import matplotlib.pyplot as plt
from pymongo import MongoClient
from datetime import datetime
import sklearn as sk
import pandas as pd
import numpy as np
import json

#MONGO_DB_INSTANCE = "localhost"
#CLIENT_NAME = "Big_Data_Project"
#COLLECTION_NAME = "Client datas"

DAGS_FOLDER = "/home/axelle/airflow/dags/"

def get_data():
    """Récupère les données dans la base de données"""
    #client = MongoClient(MONGO_DB_INSTANCE)
    #db = client[CLIENT_NAME]
    #collection = db[COLLECTION_NAME]
    df = pd.read_json(DAGS_FOLDER + "processed data.json")
    df = df.loc[::, ["amount", "datetime_timestamp"]]
    return df


def cluster_data(df):
    """Clustérise les données"""
    kmeans = KMeans(n_clusters=3, n_init=10, random_state=0).fit(df)
    return kmeans


def plot_clusters(df, kmeans):
    """Affiche les clusters"""
    plt.scatter(df['datetime_timestamp'],df['amount'], c=kmeans.labels_)
    plt.xlabel("Datetime_timestamp")
    plt.ylabel("Amount")
    plt.title("Affichage du cluster sur nos données générée")
    plt.savefig(DAGS_FOLDER + "Cluster_graph/cluster_" + datetime.now().strftime("%d-%m-%Y_%H:%M:%S") + ".png")


def main():
    df = get_data()
    kmeans = cluster_data(df)
    plot_clusters(df, kmeans)
    pass


if __name__ == '__main__':
    main()