# DataViz - Transactions bancaires (Sujet 4)

Contexte : Votre client génère des données toutes les 15 minutes qu’il va vous envoyer sur votre infrastructure. Vous devez les traiter avant le prochain batch et le mettre à disposition de votre client.

Vous aurez à designer l’architecture de votre Pipeline. Les contraintes de vos clients sont les suivantes :

- Vous devez fournir un script permettant l’ingestion des fichiers csv

- Vous devrez connecter un Power BI qui sera chargé de faire la visualisation des données processées qui se mettre à jour automatiquement. 

Un échantillon du dataset vous sera envoyé pour que vous puissez mettre en place la pipeline. 

Dans un premier temps la pipeline de processing des données ne devra faire que des statistiques descriptives simples. Une fois que vous aurez réussi à faire une pipeline ingérant les données et générant les statisitiques descriptives simples vous ajouterez un modèle de prédiction. 

Les données seront des données tabulaires. Possibilité sur demande d’avoir à process des données textuelles ou sur une thématique donnée.

## Etape 1 - Environnement

Créer une VM Linux (Nous avons utilisé Ubuntu).

Installation Airflow :

- Pour installer des packages python nous aurons besoin de la commande pip.

```
sudo apt install python3-pip
```
- Pour configurer un environnement virtuel, nous devons installer un package python nommé virtualenv.

```
sudo pip3 install virtualenv
```
- Création du répertoire d'environnement virtuel

```
virtualenv airflow_env
```

- Activation de l'environnement

```
source airflow_env/bin/activate
```

- Installation des bibliothèques Airflow et Essential :

```
pip3 install apache-airflow[gcp,sentry,statsd]
pip3 install pyspark
pip3 install sklearn
```

-Initialisation de la base de données :

```
cd airflow
airflow db init
```

- Création dossier "dags" :

```
mkdir dags
```

- Création d'un nouvel utilisateur Airflow :

```
airflow users create --username admin --password your_password --firstname your_first_name --lastname your_last_name --role Admin --email your_email@domain.com
```

- Démarrer les servers Airflow (Dans deux terminaux différents et dans l'environnement créé au préalable):
```
airflow scheduler
airflow webserver
```

- Pour visualiser le Dashboard Airflow allez dans : http://localhost:8080/


Installer les librairies suivantes dans votre environnement pour qu'il n'y ai pas d'erreur à l'execution des scripts :

```
pip install pandas
pip install numpy
pip install matplotlib
```

Si c'est déjà le cas vous pouvez passer à l'étape suivante.

## Etape 2 - Comprendre l'architecture

![archi](/uploads/841db2602d8f1b7489a06525f6691c49/archi.PNG)

## Etape 3 - Usage

- Se connecter à sa VM Ubuntu

- Ouvrir deux terminaux

- Dans le premier terminal, executez les commandes suivantes :

```
source airflow_env/bin/activate
airflow scheduler
```

- Dans le deuxième terminal,  executez les commandes suivantes :

```
source airflow_env/bin/activate
airflow webserver
```

- Allez dans : http://localhost:8080/

- Modification à effectué sur les scripts :

    data_pipeline_dag.py & file_writer : Changer le répertoire de la variable "CSV_DIR" pour qu'il corresponde au répertoire sur votre machine où le client va déposer ses fichiers csv.

    pipeline.py & clustering.py : Changer "DAGS_FOLDER". Remplacer par le directory où vous allez stocker vos dags.